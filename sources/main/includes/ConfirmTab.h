/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#ifndef CONFIRMWIDGET_H
#define CONFIRMWIDGET_H

#include <QWidget>

#include "httraqt.h"
#include "ui_ConfirmTab.h"

class HTTraQt;

class ConfirmTab: public QWidget, public Ui::ConfirmTab, public cTranslator
{
        Q_OBJECT
    public:
        ConfirmTab(QWidget *parent = 0, Qt::WindowFlags fl = 0);
        void translateTab();

    protected:
        void resizeEvent(QResizeEvent *event);

    private slots:
        void onRadio(bool t);

    private:
        HTTraQt* parent;
};

#endif
