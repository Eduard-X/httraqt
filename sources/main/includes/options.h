/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#ifndef OPTIONS_HEADER
#define OPTIONS_HEADER


#include "htinterface.h"
// #include "htsstrings.h"

#ifdef __cplusplus
extern "C"
{
#endif

#include <httrack-library.h>
#include <htsglobal.h>
#include <htswrap.h>
#include <htsstrings.h>
#include <htsopt.h>
#include <htsdefines.h>


#ifdef __cplusplus
}
#endif

#include <QWidget>
#include <QString>
#include <QVariant>
#include <QMap>
#include <QSettings>
#include <QStringList>


class cOptions
{
    public:
        void initOptions(void);
        void loadOptions(QSettings *s);
        void saveOptions(QSettings *s, bool gl);

        void SetProfile(const QString &variName, float val);
        void SetProfile(const QString &variName, int val);
        void SetProfile(const QString &variName, const QString &val);

        void SetProfile(const QString &variName, const QVariant &val);

        void GetProfile(const QString &variName, int &val);
        void GetProfile(const QString &variName, float &val);
        void GetProfile(const QString &variName, QString &val);

        QVariant GetProfile(const QString &variName);

        QString cmdArgumentsOptions(int num);
        void    getOptStruct(httrackp *opt);

    public:
        static QMap<QString, QVariant> prOptions;
        int maxProgressRows;
        QString selectedLang;
        QString currentProject;
        QString currentWorkDir;
        QString currentAppDir;

    private:
        void StripControls(QString &st);
        void profile_code(QString &from);
        void profile_decode(QString &from);
};



#endif
