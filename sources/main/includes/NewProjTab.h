/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#ifndef NEWPROJECTWIDGET_H
#define NEWPROJECTWIDGET_H

#include "ui_NewProjTab.h"
#include "httraqt.h"


class HTTraQt;

class NewProjTab : public QWidget, public Ui::NewProjTab, public cTranslator
{
        Q_OBJECT

    public:
        NewProjTab(QWidget* parent = 0, Qt::WindowFlags fl = 0);
        ~NewProjTab();
        void translateTab();
        bool testIt();
        void rebuildDirList();
        void selectProj(const QString &name);
        /*$PUBLIC_FUNCTIONS$*/

    protected:
        QStringList currentsubdirs;
        /*$PROTECTED_FUNCTIONS$*/

    protected:
        void resizeEvent(QResizeEvent *event);

    protected slots:
        /*$PROTECTED_SLOTS$*/
    private:
        QString editedName;
        HTTraQt* parent;

    private slots:
        void workDirChanged();
        void projectInfoSelected();
        void projectInfoEntered();
        //         void onSelectBatchFile();

        void onBrowseProject();
        //     void onProjectNameChanged();
        void onDirChanged();
        void changeProjname(QString stl);
};

#endif

