/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include <QtGui>

#include "includes/StartTab.h"
#include "includes/httraqt.h"
#include "../version.h"


StartTab::StartTab(QWidget *parent, Qt::WindowFlags fl)
    : QWidget(parent, fl)
{
    setupUi(this);

    this->parent = static_cast<HTTraQt*>(parent);

    label01->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    label->setPixmap(*(this->parent->mainpix));
}


void StartTab::resizeEvent(QResizeEvent *event)
{
}

void StartTab::translateTab()
{
    setMinimumSize(410, 250);
    QString v;

    if (parent->programStyleSheet.length() > 0) {
        setStyleSheet(parent->programStyleSheet);
    }

    label01->setText(translate(_WELCOME));
    v = QString().sprintf("%s v.%s (%s)", PROGRAM_NAME, HTTQTVERSION, PROGRAM_DATE);
    l_version->setText(v);
}



