/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include <QtGui>

#include "includes/httraqt.h"
#include "includes/InsertUrlDialog.h"


InsertUrlDialog::InsertUrlDialog(QWidget *parent)
    : QDialog(parent)
{
    setupUi(this);

    this->parent = static_cast<HTTraQt*>(parent);

    connect(label2, SIGNAL(clicked()), this, SLOT(close()));
    connect(label1, SIGNAL(clicked()), this, SLOT(onOk()));

    translateDialog();
}


void InsertUrlDialog::translateDialog()
{
    if (parent->programStyleSheet.length() > 0) {
        setStyleSheet(parent->programStyleSheet);
    }

    groupBox->setTitle(translate(_AUTH));
    label1226->setText(translate(_LOGIN));
    label1227->setText(translate(_PASS));
    label2->setText(translate(_CANCEL));
    label1->setText(translate(_OK));
}


void InsertUrlDialog::onOk()
{
    m_urllogin = label1233->text();
    m_urlpass = label1232->text();
    m_urladr = label1234->text();
    accept();
}




