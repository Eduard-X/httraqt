/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include "includes/httraqt.h"
#include "includes/ConfirmTab.h"
#include "includes/buttonPanel.h"

buttonPanel::buttonPanel(QWidget *parent, Qt::WindowFlags fl)
    : QWidget(parent, fl)
{
    setupUi(this);

    this->parent = static_cast<HTTraQt*>(parent);

    connect(backButton, SIGNAL(clicked()), this, SLOT(onBack()));
    connect(nextButton, SIGNAL(clicked()), this, SLOT(onNext()));
    connect(stopButton, SIGNAL(clicked()), this, SLOT(onStop()));
    connect(helpButton, SIGNAL(clicked()), this, SLOT(onHelp()));

    onButtons(0);
}

void buttonPanel::onStop()
{
    // progress tab
    if (parent->currentTab == 4) {
        int ans = MessageBox::exec(this, translate(_STOPQ), translate(_STOPPROG), QMessageBox::Question);

        if (ans == QMessageBox::Yes) {
            parent->onStopAll();
        }
    } else {
        parent->onQuit();
    }
}


void buttonPanel::translateButtons()
{
    if (parent->programStyleSheet.length() > 0) {
        setStyleSheet(parent->programStyleSheet);
    }

    backButton->setText(translate(_PREVIOUS));
    helpButton->setText(translate(_HELP));

    switch (parent->currentTab) {
        case 0: { // start tab
            nextButton->setText(translate(_NEXT));
            stopButton->setText(translate(_EXIT));
            break;
        }

        case 1: { // select proj tab
            nextButton->setText(translate(_NEXT));
            stopButton->setText(translate(_EXIT));
            break;
        }

        case 2: { // options tab
            nextButton->setText(translate(_NEXT));
            stopButton->setText(translate(_EXIT));
            break;
        }

        case 3: { // confirm tab
            nextButton->setText(translate(_READY));
            stopButton->setText(translate(_EXIT));
            break;
        }

        case 4: { // progress tab
            nextButton->setText(translate(_NEXT));
            stopButton->setText(translate(_STOP));
            break;
        }

        case 5: { // log tab
            nextButton->setText(translate(_READY));
            stopButton->setText(translate(_EXIT));
            break;
        }

        default:
            break;
    }
}


void buttonPanel::onButtons(int n)
{
    // for: start; select proj; options; delay; progress; log pages
    unsigned char mask[8] = {0x06, 0x07, 0x07, 0x07, 0x02, 0x06, 0x00};
    unsigned char m;

    if (n < 0 || n > 5) {
        return;
    }

    m = mask[n];

    translateButtons();

    backButton->setEnabled((m & 0x01));
    stopButton->setEnabled((m & 0x02));
    nextButton->setEnabled((m & 0x04));
}


void buttonPanel::onHelp()
{
}


void buttonPanel::onBack()
{
    if (parent->currentTab == 0) {
        return;
    }

    parent->activatePage(parent->currentTab - 1);
}


void buttonPanel::onNext()
{
    if (parent->currentTab == 5) {
        parent->activatePage(0);
        return;
    }

    if (parent->currentTab == 3) {
        if (static_cast<ConfirmTab*>(parent->widgets[3])->radioStart->isChecked() == false) {
            parent->getMainOptionsFromGUI();
            parent->writeSettings(false); // write project settings
            parent->activatePage(0);
            return;
        }
    }

    parent->activatePage(parent->currentTab + 1);
}

