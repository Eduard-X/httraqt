/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include <QDesktopServices>
#include <QUrl>

#include "includes/httraqt.h"
#include "includes/FinalTab.h"


FinalTab::FinalTab(QWidget *parent, Qt::WindowFlags fl)
    : QWidget(parent, fl)
{
    setupUi(this);

    this->parent = static_cast<HTTraQt*>(parent);

    connect(label3, SIGNAL(clicked()), this, SLOT(onViewLog()));
    connect(label4, SIGNAL(clicked()), this, SLOT(onBrowseLocalWebsite()));
}


void FinalTab::resizeEvent(QResizeEvent *event)
{
}

void FinalTab::translateTab(void)
{
    setMinimumSize(410, 250);

    if (parent->programStyleSheet.length() > 0) {
        setStyleSheet(parent->programStyleSheet);
    }

    label3->setText(translate(_VIEWLOG));
    label4->setText(translate(_BROWSEWEBSITE));
}


void FinalTab::onViewLog()
{
    QDesktopServices::openUrl(QUrl::fromLocalFile(qPrintable( parent->currentWorkDir + "/" + parent->currentProject + "/hts-log.txt")));
}

void FinalTab::onBrowseLocalWebsite()
{
    QDesktopServices::openUrl(QUrl::fromLocalFile(parent->currentWorkDir + "/" + parent->currentProject + "/index.html"));
}


