/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include "includes/OptionsDialog.h"
#include "includes/optionsmime.h"
#include "../main/includes/httraqt.h"

optionsMime::optionsMime(QWidget* parent, Qt::WindowFlags fl)
    : QWidget(parent, fl), Ui::mimeForm()
{
    setupUi(this);

    this->parent = static_cast<OptionsDialog*>(parent);

    QStringList htmlMime;
    htmlMime << "" << "text/html" << "text/text" << "image/gif" << "image/jpeg" << "image/png" << "application/x-zip" << "application/x-tar.gz" << "application/x-rar" << "application/x-mp3" << "application/x-mov" << "application/octet-stream";

    ident << mimeForm::ident01 << mimeForm::ident02 << mimeForm::ident03 << mimeForm::ident04 << mimeForm::ident05 << mimeForm::ident06 << mimeForm::ident07 << mimeForm::ident08;
    mime << mimeForm::mime01 << mimeForm::mime02 << mimeForm::mime03 << mimeForm::mime04 << mimeForm::mime05 << mimeForm::mime06 << mimeForm::mime07 << mimeForm::mime08;

    for (int i = 0; i < ident.size(); i++) {
        ident[i]->insertItems(0, htmlMime);
        //         connect(ident[i], SIGNAL(activated(int)), this, SLOT(onMIME()));
    }

    QString st;
    QStringList listeMime;
    listeMime << "" << "php" << "cgi" << "asp" << "php2" << "php3" << "php4" << "php5" << "jsp" << "pl" << "cfm" << "php,php3,asp" << "asp,exe" << "pl,cgi" << "php,php2,php3,php4,php5";

    for (int i = 0; i < mime.size(); i++) {
        mime[i]->insertItems(0, listeMime);
    }

    opts = &(static_cast<OptionsDialog*>(this->parent))->_tabTextInfos;

    initTextPoints();
}

optionsMime::~optionsMime()
{
}


void optionsMime::initTextPoints()
{
    *opts << (trWidgets) {
        mimeForm::groupBox_3, _MIME_ASSOC, "", GROUPBOX,  0
    };
    *opts << (trWidgets) {
        mimeForm::label1294, _FILE_TYPES, "", LABEL,  0
    };
    *opts << (trWidgets) {
        mimeForm::label1295, _MIME_ID, "", LABEL,  0
    };

    for (int i = 0; i < 8; i++) {
        *opts << (trWidgets) {
            ident[i], -1, QString().sprintf("MIMEDefsExt%d", (i + 1)), COMBOBOX, ""
        };
        *opts << (trWidgets) {
            mime[i], -1, QString().sprintf("MIMEDefsMime%d", (i + 1)), COMBOBOX, ""
        };
    }
}

void optionsMime::onMIME()
{
    for (int i = 0; i < 8; i++) {
        if (mime[i]->currentText().length() > 0 && ident[i]->currentText().length() > 0) {
            ident[i]->setEnabled(true);
            mime[i]->setEnabled(true);

            if (i < 7) {
                ident[i + 1]->setEnabled(true);
                mime[i + 1]->setEnabled(true);
            }

            continue;
        }

        if (mime[i]->currentText().length() == 0 && ident[i]->currentText().length() == 0) {
            ident[i]->setEnabled(false);
            mime[i]->setEnabled(false);
        }
    }
}


/*$SPECIALIZATION$*/


