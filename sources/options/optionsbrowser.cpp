/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include <QVector>

#include "includes/OptionsDialog.h"
#include "includes/optionsbrowser.h"
#include "../main/includes/httraqt.h"
#include "../version.h"


optionsBrowser::optionsBrowser(QWidget* parent, Qt::WindowFlags fl)
    : QWidget(parent, fl), Ui::browserForm()
{
    setupUi(this);

    this->parent = static_cast<OptionsDialog*>(parent);

    // s. http://www.useragentstring.com/pages/useragentstring.php
    //     QStringList brws_list[6];
    QStringList os, brws;
    brws << "Firefox" // 0
         << "Chrome"  // 1
         << "Opera"   // 2
         << "MSIE"    // 3
         << "Netscape"// 4
         << "Lynx"    // 5
         << "Other";  // 6

    brws_list[0] << "Mozilla/5.0 (%s; rv:33.0) Gecko/20100101 Firefox/33.0"
                 << "Mozilla/5.0 (%s; rv:32.0) Gecko/20120101 Firefox/32.0"
                 << "Mozilla/5.0 (%s; rv:31.0) Gecko/20120101 Firefox/31.0"
                 << "Mozilla/5.0 (%s; rv:30.0) Gecko/20120101 Firefox/30.0"
                 << "Mozilla/5.0 (%s; rv:29.0) Gecko/20120101 Firefox/29.0"
                 << "Mozilla/5.0 (%s; rv:25.0) Gecko/20100101 Firefox/25.0"
                 << "Mozilla/5.0 (%s; rv:24.0) Gecko/20100101 Firefox/24.0"
                 << "Mozilla/5.0 (%s; rv:22.0) Gecko/20130405 Firefox/23.0"
                 << "Mozilla/5.0 (%s; rv:22.0) Gecko/20130405 Firefox/22.0"
                 << "Mozilla/5.0 (%s; rv:21.0) Gecko/20100101 Firefox/21.0"
                 << "Mozilla/5.0 (%s; rv:20.0) Gecko/20100101 Firefox/20.0"
                 << "Mozilla/5.0 (%s; rv:19.0.1) Gecko/20100101 Firefox/19.0.1"
                 << "Mozilla/5.0 (%s; rv:18.0) Gecko/20100101 Firefox/18.0"
                 << "Mozilla/5.0 (%s; rv:17.0.1) Gecko/20100101 Firefox/17.0.1"
                 << "Mozilla/5.0 (%s; rv:16.0) Gecko/20100101 Firefox/16.0"
                 << "Mozilla/5.0 (%s; rv:14.0) Gecko/20100101 Firefox/14.0.1"
                 << "Mozilla/5.0 (%s; rv:10.0.1) Gecko/20100101 Firefox/10.0.1"
                 << "Mozilla/5.0 (%s; rv:1.9.8) Firefox/5.0"
                 << "Mozilla/5.0 (%s; rv:1.9.6) Firefox/4.0"
                 << "Mozilla/5.0 (%s; rv:1.9.2) Firefox/3.6.0"
                 << "Mozilla/5.0 (%s; rv:1.9.0.10) Firefox/3.0.2"
                 << "Mozilla/5.0 (%s; rv:1.8.1.4) Firefox/2.0.0.3";

    brws_list[1]  << "Mozilla/5.0 (%s) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36"
                  << "Mozilla/5.0 (%s) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36"
                  << "Mozilla/5.0 (%s) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1985.67 Safari/537.36"
                  << "Mozilla/5.0 (%s) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.17 Safari/537.36"
                  << "Mozilla/5.0 (%s) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1464.0 Safari/537.36"
                  << "Mozilla/5.0 (%s) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1309.0 Safari/537.17"
                  << "Mozilla/5.0 (%s) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6"
                  << "Mozilla/5.0 (%s) AppleWebKit/535.20 (KHTML, like Gecko) Chrome/19.0.1036.7 Safari/535.20";

    brws_list[2] << "Opera/9.80 (%s) Presto/2.12.388 Version/12.14"
                 << "Mozilla/5.0 (%s) Gecko/20100101 Firefox/4.0 Opera 12.14"
                 << "Opera/9.80 (%s) Presto/2.12.388 Version/12.14"
                 << "Opera/12.0(%s;U;en)Presto/22.9.168 Version/12.00"
                 << "Opera/12.0 (%s;U;en) Presto/22.9.168 Version/12.00"
                 << "Opera/9.80 (%s; U; pt) Presto/2.10.229 Version/11.62"
                 << "Opera/9.80 (%s) Presto/2.9.168 Version/11.52"
                 << "Opera/9.20 (%s)"
                 << "Opera/9.0 (%s)";

    brws_list[3] << "Mozilla/5.0 (%s; rv:1.0.1) Netscape/7.0"
                 << "Mozilla/4.76 [en] (%s)";

    brws_list[4] << "Mozilla/5.0 (compatible; MSIE 10.0; %s; Trident/6.0)"
                 << "Mozilla/5.0 (compatible; MSIE 9.0; %s; Trident/5.0)"
                 << "Mozilla/4.0 (compatible; MSIE 5.22; %s)"
                 << "Mozilla/4.0 (compatible; MSIE 7.0; %s)"
                 << "Mozilla/4.0 (compatible; MSIE 6.0; %s)"
                 << "Mozilla/4.5 (compatible; MSIE 4.01; %s)"
                 << "Mozilla/4.78 [en] (%s; U)"
                 << "Mozilla/4.61 [en] (%s; I)"
                 << "Mozilla/2.0 (compatible; MSIE 3.01; %s)"
                 << "Mozilla/4.5 [en] (%s)"
                 << "Mozilla/4.5 (%s)"
                 << "Mozilla/3.01-C-MACOS8 (%s)"
                 << "Mozilla/2.0 (%s; I)"
                 << "Mozilla/3.0 WebTV/1.2 (compatible; MSIE 2.0)"
                 << "Mozilla/2.0 (compatible; MS FrontPage Express 2.0)";

    //     brws_list[5] << "Mozilla/4.05 [fr] (%s; I)";
    brws_list[5] << "Lynx/2.8rel.3 libwww-FM/2.14";

    brws_list[6] << "Mozilla/4.5 (compatible; HTTraQt 1.0x; %s)"
                 << "HyperBrowser (%s)"
                 //          << QString(PROGRAM_NAME + " " + PROGRAM_VERSION)
                 << QString().sprintf(PROGRAM_FULL_NAME, HTTQTVERSION) + " (offline browser; web mirror utility)";


    //     selectOnCombo ( *label1053, brws_list, "UserID" );

    os << "X11; U; Linux i686; I"
       << "X11; U; Linux x86_64"
       << "X11; U; FreeBSD i386"
       << "X11; OpenBSD amd64"
       << "X11; OpenBSD i686"
       << "X11; U; GNU/kFreeBSD i686"
       << "X11; I; Linux 2.0.34 i686"
       << "Windows 3.11"
       << "Windows 95"
       << "Win98"
       << "Windows 98"
       << "Windows; U; WinNT3.51"
       << "Windows NT 5.0"
       << "Windows NT 5.1"
       << "Windows; U; Windows NT 5.1"
       << "Windows; U; Windows NT 6.1"
       << "Windows NT 6.2; Win64; x64"
       << "Windows NT 6.1; WOW64"
       << "Windows NT 7.1"
       << "BeOS; U; BeOS BePC"
       << "Macintosh; Intel Mac OS X 10.8"
       << "Macintosh; Intel Mac OS X; U;"
       << "Macintosh; Intel Mac OS X 10.6"
       << "Macintosh; U; PPC Mac OS X 10.5"
       << "X11; U; SunOS 5.8 sun4u"
       << "Mac_PowerPC"
       << "Macintosh; I; PPC"
       << "OS/2"
       << "Cray; I; OrganicOS 9.7.42beta-27";

    browserForm::label1053_2->insertItems(0, os);

    QStringList footer;
    footer << "<!-- Mirrored from %s%s by HTTraQt Website Copier/1.x [Karbofos 2012-2017] %s -->"
           << "<!-- Page mirrored from %s%s, file %s. Archive date: %s -->"
           << "<!-- Mirrored by HTTrack Website Copier/3.x [XR/YP'2000] -->"
           << "<!-- Mirrored from %s%s by HTTrack Website Copier/3.x [XR/YP'2000] -->"
           << "<!-- Mirrored from %s%s, %s by HTTrack Website Copier/3.x [XR/YP'2000] -->"
           << "<!-- Mirrored by: HTTrack Website Copier/3.x. Site: %s. File: %s. Date: %s -->"
           << "";

    browserForm::label1054->addItems(footer);

    connect(browserForm::label1053_1, SIGNAL(currentIndexChanged ( int )), this, SLOT( brwserChanged(int)));

    opts = &(static_cast<OptionsDialog*>(this->parent))->_tabTextInfos;

    initTextPoints();
}

optionsBrowser::~optionsBrowser()
{
}


void optionsBrowser::brwserChanged(int n)
{
    browserForm::label1053->clear();
    browserForm::label1053->insertItems(0, brws_list[n]);
}


void optionsBrowser::initTextPoints()
{
    *opts << (trWidgets) {
        browserForm::label1196, _BROWS_ID, "", LABEL, 0
    };
    *opts << (trWidgets) {
        browserForm::label1196_1, _BROWS_NAME, "", LABEL, 0
    };
    *opts << (trWidgets) {
        browserForm::label1196_2, _OS_ID, "", LABEL, 0
    };
    *opts << (trWidgets) {
        browserForm::label1197, _HTML_FOOT, "", LABEL, 0
    };
    *opts << (trWidgets) {
        browserForm::label1053, -1, "BrowserID", COMBOBOX, ""
    };
    *opts << (trWidgets) {
        browserForm::label1053_1, -1, "BrowserName", COMBOBOX, ""
    };
    *opts << (trWidgets) {
        browserForm::label1053_2, -1, "osID", COMBOBOX, ""
    };
    *opts << (trWidgets) {
        browserForm::label1054, -1, "Footer", COMBOBOX, ""
    };
}

#if 0
void optionsBrowser::initTextPoints()
{
    *opts << (trWidgets) {
        browserForm::label1196, _BROWS_ID, "", LABEL,  0
    };
    *opts << (trWidgets) {
        browserForm::label1196_1, _BROWS_NAME, "", LABEL,  0
    };
    *opts << (trWidgets) {
        browserForm::label1196_2, _OS_ID, "", LABEL,   0
    };
    *opts << (trWidgets) {
        browserForm::label1197, _HTML_FOOT, "", LABEL,  0
    };
    *opts << (trWidgets) {
        browserForm::label1053, -1, "BrowserID", COMBOBOX,  ""
    };
    *opts << (trWidgets) {
        browserForm::label1053_1, -1, "BrowserName", COMBOBOX,  ""
    };
    *opts << (trWidgets) {
        browserForm::label1053_2, -1, "osID", COMBOBOX, ""
    };
    *opts << (trWidgets) {
        browserForm::label1054, -1, "Footer", COMBOBOX, ""
    };
}
#endif
/*$SPECIALIZATION$*/


