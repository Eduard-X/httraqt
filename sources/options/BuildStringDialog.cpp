/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include <QtGui>


#include "../main/includes/httraqt.h"
#include "includes/BuildStringDialog.h"


BuildStringDialog::BuildStringDialog(QWidget *parent)
    : QDialog(parent)
{
    setupUi(this);

    this->parent = static_cast<HTTraQt*>(parent);

    initTextPoints();

    setWindowTitle(translate(_USERDEFSTRUCT));
    connect(okButton, SIGNAL(clicked()), this, SLOT(onOk()));
    connect(pushButton, SIGNAL(clicked()), this, SLOT(reject()));
}


void BuildStringDialog::onOk()
{
    QString st;
    st = lineEdit->text();
    parent->SetProfile("BuildString", st);

    accept();
}


void BuildStringDialog::initTextPoints()
{
    QString st;
    label->setText(translate(_Q2));
    label_2->setText(translate(_Q3));

    parent->GetProfile("BuildString", st);
    lineEdit->setText(st);
}






