/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include "includes/OptionsDialog.h"
#include "includes/optionslimits.h"
#include "../main/includes/httraqt.h"

optionsLimits::optionsLimits(QWidget* parent, Qt::WindowFlags fl)
    : QWidget(parent, fl), Ui::limitsForm()
{
    setupUi(this);

    this->parent = static_cast<OptionsDialog*>(parent);

    QStringList maxTime;
    maxTime << "" << "120" << "900" << "1800" << "3600" << "7200";
    limitsForm::comboMaxTime->insertItems(0, maxTime);    // max time overall
    limitsForm::comboMaxTime->setDuplicatesEnabled(false);

    connect (limitsForm::comboMaxTime->lineEdit(), SIGNAL(editingFinished()), this, SLOT(editingFinished()));

    QStringList connSec;
    connSec << "" << "1" << "2" << "4" << "8" << "16" << "32" << "64" << "128";
    limitsForm::comboMaxConn->insertItems(0, connSec);    // max connections/sec
    limitsForm::comboMaxConn->setDuplicatesEnabled(false);

    connect (limitsForm::comboMaxConn->lineEdit(), SIGNAL(editingFinished()), this, SLOT(editingFinished()));

    limitsForm::comboMaxDep->insertItems(0, connSec); // max depth
    limitsForm::comboMaxDep->setDuplicatesEnabled(false);

    connect (limitsForm::comboMaxDep->lineEdit(), SIGNAL(editingFinished()), this, SLOT(editingFinished()));

    QStringList extD;
    extD << "" << "0" << "1" << "2" << "4" << "8" << "16" << "32" << "64" << "128";
    limitsForm::comboMaxExtDep->insertItems(0, extD); // max links
    limitsForm::comboMaxExtDep->setDuplicatesEnabled(false);
    connect (limitsForm::comboMaxExtDep->lineEdit(), SIGNAL(editingFinished()), this, SLOT(editingFinished()));

    QStringList maxLinks;
    maxLinks << "" << "100000" << "200000" << "1000000" << "5000000";
    limitsForm::comboMaxLinks->insertItems(0, maxLinks);    // max links
    limitsForm::comboMaxLinks->setDuplicatesEnabled(true);

    connect (limitsForm::comboMaxLinks->lineEdit(), SIGNAL(editingFinished()), this, SLOT(editingFinished()));

    QStringList maxTransferRate;
    maxTransferRate << "" << "0.0001" << "0.001" << "0.01" << "0.1" << "0.5" << "1" << "5" << "20" << "50" << "75" << "100" << "250";
    limitsForm::comboMaxTransf->insertItems(0, maxTransferRate);   // max transfer rate
    limitsForm::comboMaxTransf->setDuplicatesEnabled(false);

    connect (limitsForm::comboMaxTransf->lineEdit(), SIGNAL(editingFinished()), this, SLOT(editingFinished()));

    opts = &(static_cast<OptionsDialog*>(this->parent))->_tabTextInfos;

    initTextPoints();

    limitsForm::labelMaxHtml->setText("MB");
    limitsForm::labelMaxNHtml->setText("MB");
    limitsForm::labelMaxSize->setText("MB");
    limitsForm::labelMaxTransf->setText("MB/s");
}


optionsLimits::~optionsLimits()
{
}


// check the entered value depended from combobox
void optionsLimits::editingFinished()
{
    QLineEdit* from;
    bool valGood;
    QString t;
    from = (QLineEdit*)sender();
    QComboBox *par = (QComboBox*)from->parent();
    t = from->text();

    //     qDebug() << from << t;
    if (t == "" || t == "\n") {
        par->setCurrentIndex(0); // first element, = -1
        return;
    }

    disconnect (from, SIGNAL(editingFinished()), this, SLOT(editingFinished()));

    if (from == limitsForm::comboMaxConn->lineEdit()) { // int, range: space, 0..255
        int tmp = t.toInt(&valGood);

        if (valGood == true) {
            if (!(tmp >= 0 && tmp <= 255)) {
                qDebug() << "wrong" << from << tmp;
                valGood = false;
            }
        }
    }

    if (from == limitsForm::comboMaxDep->lineEdit()) { // int, range: space, 0..9999
        int tmp = t.toInt(&valGood);

        if (valGood == true) {
            if (!(tmp >= 0 && tmp <= 9999)) {
                qDebug() << "wrong" << from << tmp;
                valGood = false;
            }
        }
    }

    if (from == limitsForm::comboMaxExtDep->lineEdit()) { // int, range: space, 0..9999
        int tmp = t.toInt(&valGood);

        if (valGood == true) {
            if (!(tmp >= 0 && tmp <= 9999)) {
                qDebug() << "wrong" << from << tmp;
                valGood = false;
            }
        }
    }

    if (from == limitsForm::comboMaxTime->lineEdit()) { // int, range: space, 0..99999 sec
        int tmp = t.toInt(&valGood);

        if (valGood == true) {
            if (!(tmp >= 0 && tmp <= 99999)) {
                qDebug() << "wrong" << from << tmp;
                valGood = false;
            }
        }
    }

    if (from == limitsForm::comboMaxTransf->lineEdit()) { // float, range: space, 0..1000 MB/s
        float tmp = t.toFloat(&valGood);

        if (valGood == true) {
            if (!(tmp >= 0 && tmp <= 1000)) {
                qDebug() << "wrong" << from << tmp;
                valGood = false;
            }
        }
    }

    if (from == limitsForm::comboMaxLinks->lineEdit()) { // int, range: space, 0..5000000
        int tmp = t.toInt(&valGood);

        if (valGood == true) {
            if (!(tmp >= 0 && tmp <= 5000000)) {
                qDebug() << "wrong" << from << tmp;
                valGood = false;
            }
        }
    }

    if (valGood == true) {
        //         qDebug() << "right" << t;
        int currInd = par->findText(t);

        if ( currInd < 0) {
            par->addItem(t);
            par->model()->sort(0);
            int tInd = par->findText(t);
            par->setCurrentIndex(tInd);
        } else {
            par->setCurrentIndex(currInd);
        }
    }

    connect (from, SIGNAL(editingFinished()), this, SLOT(editingFinished()));
}


void optionsLimits::initTextPoints()
{
    *opts << (trWidgets) {
        limitsForm::label1141, _MAX_FROM_ROOT, "", LABEL, 0
    };
    *opts << (trWidgets) {
        limitsForm::label1143, _MAX_EXTERNAL, "", LABEL, 0
    };
    *opts << (trWidgets) {
        limitsForm::label1205, _MAX_SIZE_HTML, "", LABEL, 0
    };
    *opts << (trWidgets) {
        limitsForm::label1209_2, _MAX_SIZE_NHTML, "", LABEL, 0
    };
    *opts << (trWidgets) {
        limitsForm::label1206, _SIZE_LIMIT, "", LABEL, 0
    };
    *opts << (trWidgets) {
        limitsForm::label1210, _PAUSE_DOWN, "", LABEL, 0
    };
    *opts << (trWidgets) {
        limitsForm::label1207, _MAX_TIME, "", LABEL, 0
    };
    *opts << (trWidgets) {
        limitsForm::label1208, _MAX_RATE, "", LABEL, 0
    };
    *opts << (trWidgets) {
        limitsForm::label1211, _MAX_CONN_SEC, "", LABEL, 0
    };
    *opts << (trWidgets) {
        limitsForm::label1212, _MAX_LINKS, "", LABEL, 0
    };
    *opts << (trWidgets) {
        limitsForm::comboMaxDep, -1, "Depth", COMBOBOX, 0
    };
    *opts << (trWidgets) {
        limitsForm::comboMaxExtDep, -1, "ExtDepth", COMBOBOX, 0
    };
    *opts << (trWidgets) {
        limitsForm::comboMaxTime, -1, "MaxTime", COMBOBOX, 0
    };
    *opts << (trWidgets) {
        limitsForm::comboMaxTransf, -1, "MaxRate", COMBOBOX, 0.0
    };
    *opts << (trWidgets) {
        limitsForm::comboMaxConn, -1, "MaxConn", COMBOBOX, 0
    };
    *opts << (trWidgets) {
        limitsForm::comboMaxLinks, -1, "MaxLinks", COMBOBOX, 0
    };
    *opts << (trWidgets) {
        limitsForm::editMaxHtml, -1, "MaxHtml", EDITLINE, 0.0
    };
    *opts << (trWidgets) {
        limitsForm::editMaxNHtml, -1, "MaxOther", EDITLINE, 0.0
    };
    *opts << (trWidgets) {
        limitsForm::editMaxSize, -1, "MaxAll", EDITLINE, 0.0
    };
    *opts << (trWidgets) {
        limitsForm::editPause, -1, "MaxWait", EDITLINE, 0
    };
}


/*$SPECIALIZATION$*/


