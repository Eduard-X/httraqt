/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#include <QWidget>
#include <QString>
#include <QVariant>

#ifndef OPTIONS_H
#define OPTIONS_H

struct trWidgets {
    QWidget*  wg;
    short     idTr; // for translations
    QString   idString;
    short     wtype; // EDITLINE, TEXTEDIT, CHECKBOX, LABEL, RADIO, COMBOBOX
    //     short     opttype; // bool, num or string
    //     short     dummy;
    QVariant  value; // value or wrap length for GUI
};

#endif

#ifndef OPTIONSDIALOG_H
#define OPTIONSDIALOG_H

#include "../../main/includes/httraqt.h"
#include "../../main/includes/options.h"

#include "ui_OptionsDialog.h"
// #include "ui_optionsBuild.h"

#include "optionslinks.h"
#include "optionslimits.h"
#include "optionsproxy.h"
#include "optionsbuild.h"
#include "optionsrulez.h"
#include "optionsexperts.h"
#include "optionslog.h"
#include "optionsspider.h"
#include "optionsbrowser.h"
#include "optionsflow.h"
#include "optionsmime.h"

#define OPTION_SITES 11

// #define NOWRAP   -1

#define CHECKBOX 0
#define LABEL    1
#define GROUPBOX 2
#define RADIO    4
#define COMBOBOX 5
#define BUTTON   6
#define EDITLINE 7
#define TEXTEDIT 8


class optionsBuild;
class optionsBrowser;
class optionsFlow;
class optionsLimits;
class optionsLinks;
class optionsProxy;
class optionsRulez;
class optionsExperts;
class optionsLog;
class optionsSpider;
class optionsMime;


class OptionsDialog: public QDialog, public Ui::OptionsDialog, public cTranslator, public cOptions
{
        Q_OBJECT
    public:
        OptionsDialog(QWidget * parent = 0, Qt::WindowFlags f = 0);
        ~OptionsDialog();

        void onSetDefOptions();
        void setToGUI();
        void getFromGUI();
        void initOptionsDialog();
        void setFontForWidgets(void);

    private slots:
        void onHelp();
        void onOk();
        void onSaveDefOptions();
        void onResetDefOptions();
        void switchPages(int n);

    private:
        void insertInCombo( QComboBox &b, const QString &s);

    private:
        optionsBuild*   pBuild;
        optionsBrowser* pBrowser;
        optionsFlow*    pFlow;
        optionsLimits*  pLimits;
        optionsLinks*   pLinks;
        optionsProxy*   pProxy;
        optionsRulez*   pRulez;
        optionsExperts* pExperts;
        optionsLog*     pLog;
        optionsSpider*  pSpider;
        optionsMime*    pMime;

        // max OPTION_SITES
        QWidget*        pages[OPTION_SITES];
        static int      headers[OPTION_SITES];

        QString         okText;
        QString         helpText;
        QString         cancelText;

    public:
        QVector<trWidgets> _tabTextInfos;
        HTTraQt*        parent;
};

#endif
