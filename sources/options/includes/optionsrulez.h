/***************************************************************************
 * C++ Implementation:                                                     *
 * Copyright (C) 2012-2017 by Eduard Kalinowski                            *
 * Germany, Lower Saxony, Hanover                                          *
 * eduard_kalinowski@yahoo.de                                              *
 *                                                                         *
 * HTTraQt is free software; may be distributed and/or modified under the  *
 * terms of the GNU General Public License version 3 as published by the   *
 * Free Software Foundation and appearing in the file LICENSE_GPLv3        *
 * included in the packaging of this file.                                 *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU Lesser General Public        *
 * License along with HTTraQt. If not, see  http://www.gnu.org/licenses    *
 ***************************************************************************/

#ifndef OPTIONSRULEZ_H
#define OPTIONSRULEZ_H

#include <QWidget>
#include <QString>

#include "ui_OptionsRulez.h"
#include "./OptionsDialog.h"

class OptionsDialog;

class optionsRulez : public QWidget, private Ui::rulezForm
{
        Q_OBJECT

    public:
        optionsRulez(QWidget* parent = 0, Qt::WindowFlags fl = 0);
        ~optionsRulez();
        /*$PUBLIC_FUNCTIONS$*/

    private:
        //         void getScanRulezFromGUI();
        void initTextPoints();
        void detectTypeInc(const QString &st, int i);
        void onChangeLinks(bool incl);
        //     public slots:
        /*$PUBLIC_SLOTS$*/

    protected:
        /*$PROTECTED_FUNCTIONS$*/
        OptionsDialog* parent;

    protected slots:
        void setScanRulezToGUI();
        void onInc();
        void onIncludedLinks();
        void onExcludedLinks();
        /*$PROTECTED_SLOTS$*/

    private:
        QVector<trWidgets>* opts;

        QString ext[4];
        QCheckBox* chk[4];
};

#endif

