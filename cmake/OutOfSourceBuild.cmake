# Disallow in-source build
STRING(COMPARE EQUAL "${CMAKE_SOURCE_DIR}" "${CMAKE_BINARY_DIR}" HTTRAQT_IN_SOURCE)
IF(HTTRAQT_IN_SOURCE)
    MESSAGE(FATAL_ERROR "HTTraQt requires an out of source build. Please create a separate build directory and run 'cmake path_to_httraqt [options]' there.")
ENDIF(HTTRAQT_IN_SOURCE)

