#!/bin/bash

for pic in *.png
do
  echo "converting $pic"
  convert $pic $(basename $pic .png).xpm
done